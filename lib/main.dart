import 'package:assesm/addtask.dart';
import 'package:assesm/completedtask.dart';
import 'package:assesm/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'blocc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TodoTasksBloc(),
      child: MaterialApp(
        title: 'Earn Your Keep',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: TaskList(),
      ),
    );
  }
}

class TaskList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TodoTasksBloc>(context).add(LoadTodosTask());

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text("Earn Your Keep"),
          ),
          BlocBuilder<TodoTasksBloc, TaskState>(
            builder: (context, state) {
              List<Task> tasks;

              if (state is TaskInfo) {
                tasks = state.completed_tasks;
              } else {
                tasks = List.empty();
              }

              return SliverToBoxAdapter(
                child: ReccurredPoints(true, tasks: tasks),
              );
            },
            buildWhen: (previous, current) => current is TaskInfo,
          ),
          SliverPadding(padding: const EdgeInsets.all(10)),
          BlocBuilder<TodoTasksBloc, TaskState>(
            builder: (context, state) {
              List<Task> tasks;

              if (state is TaskInfo) {
                tasks = state.todo_tasks;
              } else {
                tasks = List.empty();
              }
              int animateid;
              if (state is TaskAdded) {
                animateid = state.task_added_id;
              }

              return SliverList(
                  delegate: SliverChildListDelegate(tasks.map((task) {
                if (task.task_id == animateid) {
                  return TweenAnimationBuilder(
                    tween: Tween(begin: 0.0, end: 1.0),
                    duration: Duration(milliseconds: 2000),
                    builder: (context, position, child) {
                      return Opacity(
                        opacity: position,
                        child: TodoTaskItem(task),
                      );
                    },
                  );
                } else {
                  return TodoTaskItem(task);
                }
              }).toList()));
            },
            buildWhen: (previous, current) => current is TaskInfo,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => AddTaskPage()));
        },
        tooltip: 'Add Task',
        child: Icon(Icons.add),
      ),
    );
  }
}

class TodoTaskItem extends StatelessWidget {
  final Task task;
  const TodoTaskItem(
    this.task, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: CircleAvatar(
          child: Text(task.reward.toString()),
        ),
        title: Text(task.task_description),
        trailing: IconButton(
            icon: Icon(
              Icons.check,
            ),
            onPressed: () {
              BlocProvider.of<TodoTasksBloc>(context)
                  .add(DoneTask(task.task_id));
              showDialog(
                  context: context,
                  barrierDismissible: true,
                  child: SimpleDialog(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(
                          Icons.check_circle,
                          size: 100,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                      Container(
                        child: Text(
                          task.reward.toString(),
                          style: Theme.of(context).textTheme.headline2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          "${task.task_description} Completed!",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ));
            }),
      ),
    );
  }
}

class ReccurredPoints extends StatelessWidget {
  final bool showcompleted;
  const ReccurredPoints(
    this.showcompleted, {
    Key key,
    @required this.tasks,
  }) : super(key: key);

  final List<Task> tasks;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.redeem,
              size: 60,
            ),
            Text('Recurred Points'),
            Text(
              tasks
                  .fold(
                      0,
                      (previousValue, element) =>
                          previousValue + element.reward)
                  .toString(),
              style: Theme.of(context).textTheme.headline1,
            ),
            if (this.showcompleted)
              RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => CompletedPage()));
                },
                child: Text("Completed Tasks"),
              )
          ],
        ),
      ),
    );
  }
}
