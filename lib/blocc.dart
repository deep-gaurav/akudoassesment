import 'package:assesm/models.dart';
import 'package:bloc/bloc.dart';

abstract class _TaskEvent {}

class LoadTodosTask extends _TaskEvent {}

class DoneTask extends _TaskEvent {
  final int task_id;
  DoneTask(this.task_id);
}

class AddTask extends _TaskEvent {
  final String task_description;
  final int task_reward;

  AddTask(this.task_description, this.task_reward);
}

class UndoTask extends _TaskEvent {
  final int task_id;
  UndoTask(this.task_id);
}

class TaskState {}

class TaskInfo extends TaskState {
  final List<Task> todo_tasks;
  final List<Task> completed_tasks;
  TaskInfo(this.todo_tasks, this.completed_tasks);
}

class TaskDone extends TaskInfo {
  final int last_done_task;
  TaskDone(todo_task, completed_task, this.last_done_task)
      : super(todo_task, completed_task);
}

class TaskAdded extends TaskInfo {
  final int task_added_id;
  TaskAdded(todo, completed, this.task_added_id) : super(todo, completed);
}

class TodoTasksBloc extends Bloc<_TaskEvent, TaskState> {
  TodoRepo repo = TodoRepo();
  TodoTasksBloc() : super(TaskState());
  @override
  Stream<TaskState> mapEventToState(_TaskEvent event) async* {
    if (event is LoadTodosTask) {
      yield TaskInfo(repo.tasks_todo(), repo.tasks_done());
    } else if (event is DoneTask) {
      repo.complete_task(event.task_id);
      yield TaskDone(repo.tasks_todo(), repo.tasks_done(), event.task_id);
    } else if (event is AddTask) {
      int id = repo.add_task(event.task_description, event.task_reward);
      yield TaskAdded(repo.tasks_todo(), repo.tasks_done(), id);
    } else if (event is UndoTask) {
      repo.undo_task(event.task_id);
      yield TaskInfo(repo.tasks_todo(), repo.tasks_done());
    }
  }
}
