import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocc.dart';

class AddTaskPage extends StatefulWidget {
  @override
  _AddTaskPageState createState() => _AddTaskPageState();
}

class _AddTaskPageState extends State<AddTaskPage> {
  final _formKey = GlobalKey<FormState>();

  int rewards;
  String description;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Task"),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Icon(
                Icons.redeem,
                size: 80.0,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration:
                            InputDecoration(labelText: "Task Description"),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Some Description';
                          }
                          return null;
                        },
                        onSaved: (newValue) => description = newValue,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: "Task Reward"),
                        onSaved: (newValue) =>
                            rewards = int.tryParse(newValue) ?? 0,
                        validator: (value) {
                          if (int.tryParse(value) == null) {
                            return 'Enter valid numbers';
                          } else {
                            return null;
                          }
                        },
                      ),
                    )
                  ],
                ),
              ),
              BlocListener<TodoTasksBloc, TaskState>(
                listener: (context, state) {
                  if (state is TaskAdded) {
                    Navigator.pop(context);
                  }
                },
                child: Container(),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();
            BlocProvider.of<TodoTasksBloc>(context)
                .add(AddTask(description, rewards));
          }
        },
        child: Icon(Icons.check),
      ),
    );
  }
}
