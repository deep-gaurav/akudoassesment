import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocc.dart';
import 'main.dart';
import 'models.dart';

class CompletedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TodoTasksBloc>(context).add(LoadTodosTask());

    return Scaffold(
      appBar: AppBar(
        title: Text("Completed Tasks"),
      ),
      body: SafeArea(child:
          BlocBuilder<TodoTasksBloc, TaskState>(builder: (context, state) {
        List<Task> tasks;

        if (state is TaskInfo) {
          tasks = state.completed_tasks;
        } else {
          tasks = List.empty();
        }

        return ListView(
          shrinkWrap: true,
          children: [
            ReccurredPoints(false, tasks: tasks),
            ...tasks.map((task) => Card(
                  child: ListTile(
                    leading: CircleAvatar(
                      child: Text(task.reward.toString()),
                    ),
                    title: Text(task.task_description),
                    trailing: IconButton(
                        icon: Icon(
                          Icons.undo,
                        ),
                        onPressed: () {
                          BlocProvider.of<TodoTasksBloc>(context)
                              .add(UndoTask(task.task_id));
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text("Undo ${task.task_description}")));
                        }),
                  ),
                ))
          ],
        );
      })),
    );
  }
}
