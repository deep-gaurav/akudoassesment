class Task {
  final int task_id;
  final String task_description;
  final int reward;
  bool is_completed = false;

  Task(this.task_id, this.task_description, this.reward);

  Task set_completed() {
    this.is_completed = true;
    return this;
  }

  Task set_undo() {
    this.is_completed = false;
    return this;
  }
}

class TaskRepo {
  static Map<int, Task> task_map = {
    1: Task(1, 'do this', 100),
    2: Task(2, 'this is done', 50).set_completed()
  };

  Task get_task(int task_id) {
    return task_map[task_id];
  }

  List<Task> all_tasks() {
    return task_map.values.toList();
  }

  int add_task(String task_description, int reward) {
    int task_id = task_map.keys.length + 1;
    task_map[task_id] = Task(task_id, task_description, reward);
    return task_id;
  }
}

class TodoRepo {
  List<Task> tasks_todo() {
    return TaskRepo()
        .all_tasks()
        .where((task) => task.is_completed == false)
        .toList();
  }

  List<Task> tasks_done() {
    return TaskRepo().all_tasks().where((task) => task.is_completed).toList();
  }

  int add_task(String task_description, int reward) {
    return TaskRepo().add_task(task_description, reward);
  }

  bool complete_task(int task_id) {
    TaskRepo().get_task(task_id).set_completed();
    return true;
  }

  bool undo_task(int task_id) {
    TaskRepo().get_task(task_id).set_undo();
    return true;
  }
}
